import os
import sys
import subprocess
import re

PART2_SOURCE_PATH = "parts/part2"
PART2_TESTS_PATH = "tests/"
PART2_EXECUTABLE = "simplc"
C_FAIL = '\033[91m'
C_OKGREEN = '\033[92m'
C_ENDC = '\033[0m'

RESULT_PATTERN = re.compile("[<](?P<id>.*?)[>]")
RESULT_PATTERN2 = re.compile("(?P<id>\d+)\{(?P<output>.*?)\}\s");

def print_pass(string):
	print C_OKGREEN + string + C_ENDC

def print_fail(string):
	print C_FAIL + string + C_ENDC

def get_files(path):
	files = os.listdir(path)
	files = [os.path.join(path,i) for i in files]
	return files

def get_prototype_results(path_to_file):
	'''Returns a list of expected prototypes'''
	f = open(path_to_file, 'r')
	line = f.readline()
	f.close()
	line = line.strip()
	line = line[1:]
	expect = RESULT_PATTERN.findall(line)
	return expect;

def get_prototypes_postfix(path, postfix):
	'''Returns list of (file_path, expected_output)'''
	final = []
	for i in os.listdir(path):
		if (not i.endswith(postfix)): continue
		rp = os.path.join(path, i)
		prototypes = get_prototype_results(rp);
		final.append((rp, prototypes))
	return final

def get_pass(path, postfix="P.simpl"):
	return get_prototypes_postfix(path, postfix)

def get_fail(path, postfix="F.simpl"):
	return get_prototypes_postfix(path, postfix)

def run_command(program, *args):
	program = [program]
	program.extend(args)
	try:
		try:
			output = subprocess.check_output(program)
			output = RESULT_PATTERN.findall(output)
			return (0, output)
		except subprocess.CalledProcessError as e:
			return (e.returncode, RESULT_PATTERN.findall(e.output))
	except KeyboardInterrupt as e:
		return (-129987, "")

def pre_test():
	old = os.getcwd()
	import shutil
	#shutil.copy(PART2_MAKEFILE,os.path.join(PART2_SOURCE_PATH))
	os.chdir(PART2_SOURCE_PATH)
	# should put the make command here / make our own make file to compile
	os.chmod(PART2_EXECUTABLE, 0755)
	os.chdir(old)

def column_string(a, b, width):
	output = ""
	if(not a):
		# Added extra padding to fix no a
		output += C_FAIL + " "*width + "|" + b.rjust(width) + C_ENDC
		return output
	if(not b):
		output = a.ljust(width) + "|"
		return output
	if(a == b):
		return C_OKGREEN + a.ljust(width) + "|" + b.rjust(width) + C_ENDC
	else:
		return C_FAIL + a.ljust(width) + "|" + b.rjust(width) + C_ENDC


def pretty_print(alist, blist, ahead="EXPECTED", bhead="BUT GOT", heading=""):
	bar = lambda s: "="*(s+2)*2
	ia = 0
	ib = 0
	amax = 8
	for i in alist:
		amax = max(amax,len(i))
	for i in blist:
		amax = max(amax, len(i))
	amax = max(amax, len(heading))
	amax += 10
	
	print bar(amax)
	print heading.center(amax*2)
	print bar(amax)
	print ahead.ljust(amax) + "|" + bhead.rjust(amax)
	print bar(amax)
	
	while(True):
		if(ia >= len(alist) and ib >= len(blist)):
			break
		if(ia >= len(alist) and ib < len(blist)):
			print column_string(None, blist[ib], amax)
			ib+=1
			continue
		if(ib >= len(blist) and ia < len(alist)):
			print column_string(alist[ia], None, amax)
			ia+=1
			continue
		print column_string(alist[ia], blist[ib], amax)
		ia += 1
		ib += 1
	print bar(amax)

class Part2Tests():
	program = os.path.join(PART2_SOURCE_PATH, PART2_EXECUTABLE)
	tests = PART2_TESTS_PATH

	def assertEqual(self, expected, got):
		return expected == got
	def assertNotEquals(self, expected, got):
		return expected != got

	def test_pass(self):
		expected = get_pass(self.tests)
		passed = 0;
		count = 0;
		for count, p in enumerate(expected):
				result = run_command(self.program, p[0])
				if (result[0]==-129987):
					print_fail("[PREMATURE TEST EXIT ^C]: {}".format(p[0]))
					continue
				if (not self.assertEqual(0, result[0])):
					print_fail("[FAIL ON RETURN P]: {}, {} != {}".format(p[0], result[0], 0))
					pretty_print(p[1], result[1], heading="OUTPUT THUS FAR")
					continue
				if(not self.assertEqual(p[1], result[1])):
					print_fail("\n[FAIL ON OUTPUT P]: {}".format(p[0]));
					pretty_print(p[1], result[1], heading="OUTPUT TABLE")
					continue
				print_pass("[PASS_P]: {}".format(p[0]))
				passed += 1
		return (passed, len(expected))
	
	def test_fail(self):
		passed = 0;
		count = 0;
		expected = get_fail(self.tests)
		for count, p in enumerate(expected):
			result = run_command(self.program, p[0])
			if result[0] != 2:
					print_fail("[FAIL ON RETURN F]: {}".format(p[0]))
			else:
				print_pass("[PASS_F]: {}".format(p[0]));
				passed += 1
		return(passed, len(expected))
	
	def run(self):
		returned = self.test_pass()
		r = self.test_fail()
		print_pass("*********** PASSED: {} / {}".format(returned[0]+r[0], returned[1]+r[1]))

if __name__ == "__main__":
	pre_test()
	test = Part2Tests()
	test.run()
